# Logistic Regression

# Importing the libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score,roc_auc_score,confusion_matrix,roc_curve,precision_score

# Reading the Training Dataset
dataset_train = pd.read_excel("Sample_Training_Data.xlsx")

# Separating the dependent and independent variables from Training Dataset
X_train = dataset_train.iloc[:, [1,2]].values
y_train = dataset_train.iloc[:,3].values

# Scaling the data
X_train_scaling = StandardScaler()
X_train = X_train_scaling.fit_transform(X_train)

# Fitting the Logistic Regression Model to the Training Data
classification = LogisticRegression(random_state=0)
classification.fit(X_train, y_train)

# Predicting the Sample Training Data
X_train_predict = classification.predict(X_train)

# Accuracy Score
X_train_accuracy = accuracy_score(y_true=y_train, y_pred=X_train_predict)
print(f"The Accuracy of the model is {X_train_accuracy}")

# Confusion Matrix
X_train_cm = confusion_matrix(y_true=y_train, y_pred=X_train_predict)
print("Confusion Matrix is")
print(X_train_cm)
print(f"True Positives are {X_train_cm[1][1]}")
print(f"False Positives are {X_train_cm[0][1]}")
print(f"True Negatives are {X_train_cm[0][0]}")
print(f"False Negatives are {X_train_cm[1][0]}")

# Precision Score
X_train_precision = precision_score(y_true=y_train, y_pred=X_train_predict)
print(f"The Precision of model is {X_train_precision}")

# Area Under Curve
X_train_auc = roc_auc_score(y_train, X_train_predict)
print(f"The Area Under Curve of model is {X_train_auc}")

# ROC Visualization
fpr_logistic, tpr_logistic, thresholds_logistic = roc_curve(y_train, X_train_predict)

plt.figure(figsize=(10, 7))
plt.plot(fpr_logistic, tpr_logistic, label=f"Area Under Curve for logistic = {X_train_auc} %")
plt.legend(loc=0, shadow=True, fontsize='x-large')
plt.xlabel("False Positives", fontsize='x-large', color='brown')
plt.ylabel("True Positives", fontsize='x-large', color='brown')
plt.title("ROC Curve for Logistic Regression Model", fontsize='x-large', color='green')
plt.show()

# Reading the Test Dataset
dataset_test = pd.read_excel("Sample_Testing_Data.xlsx")

# Take the Independent Variable from Test Dataset
X_test = dataset_test.iloc[:, [1, 2]].values

# Scaling the Test Data before Prediction
X_test_scaling = StandardScaler()
X_test_scale = X_test_scaling.fit_transform(X_test)

# Predicting the Sample Test Data
X_test_predict = classification.predict(X_test_scale)

print()
print("Predicted Test Set Data Results")
print()
for i in range(0, len(X_test_predict)):
    print(f"User Id - {str(dataset_test.iloc[i][0]).replace('.0', '')} whether purchase card {'YES' if X_test_predict[i] == 1 else 'NO'}")