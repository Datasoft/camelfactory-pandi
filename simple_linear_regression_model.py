# Simple Linear Regression

# Importing the libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error

# Reading the Training Dataset
dataset_train = pd.read_excel("sample_data_training.xlsx")

# Separating Dependent and Independent Variables
X_train = dataset_train.iloc[:, -1:].values
y_train = dataset_train.iloc[:, 0].values

# Fitting Simple Linear Regression to the Training Data
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predicting the Same Training Data to see the best fit line
X_train_predict = regressor.predict(X_train)

# Model Equation
print(f'The model equation => Profit^ = {regressor.intercept_} + {regressor.coef_[0]} (Reasearch and Development) + Error')

# Error
residuals_predict_X_train = y_train - X_train_predict
# print(residuals_predict_X_train)

# R Square for X Train
R_Square_X_train = r2_score(X_train_predict, y_train)
print(f"R Square value of Training Data Model is {R_Square_X_train}")

# RMSE of Train
rmse_train = np.sqrt(mean_squared_error(y_train, X_train_predict))
print(f"The RMSE of Training Data is {rmse_train}")
print()
print()

# Visualizing the Training Datasets
plt.scatter(X_train, y_train, color="red")
plt.plot(X_train, X_train_predict, color="blue")
plt.title('Profit Vs Reasearch and Development Expenditure (Training Set)')
plt.xlabel('Research and Development Expenditure')
plt.ylabel('Profit for Company')
plt.show()

# Reading the test dataset
dataset_test = pd.read_excel("sample_data_test.xlsx")

# Taking the Independent variable from Test Dataset
X_test = dataset_test.iloc[:, 0:].values

# Predicting the Test Data
X_test_predict = regressor.predict(X_test)

print("Predicted Test Data set Results :")
print()
for i in range(0, len(X_test_predict)):
    print(f"The predicted profit for {X_test[i][0]} in Lakh(s) invested in Reasearch and Developemnt is {X_test_predict[i]} crore(s)")
    print()